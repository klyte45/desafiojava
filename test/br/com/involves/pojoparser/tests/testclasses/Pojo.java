/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.involves.pojoparser.tests.testclasses;

/**
 *
 * @author Leandro
 */
public class Pojo {

    private String atribute1;
    private int atribute2;
    private long atribute3;

    public String getAtribute1() {
        return atribute1;
    }

    public Pojo(String atribute1, int atribute2, Long atribute3) {
        this.atribute1 = atribute1;
        this.atribute2 = atribute2;
        this.atribute3 = atribute3;
    }

    public void setAtribute1(String atribute1) {
        this.atribute1 = atribute1;
    }

    public int getAtribute2() {
        return atribute2;
    }

    public void setAtribute2(int atribute2) {
        this.atribute2 = atribute2;
    }

    public long getAtribute3() {
        return atribute3;
    }

    public void setAtribute3(long atribute3) {
        this.atribute3 = atribute3;
    }
}
