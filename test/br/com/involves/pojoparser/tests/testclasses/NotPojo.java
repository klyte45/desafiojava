/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.involves.pojoparser.tests.testclasses;

/**
 *
 * @author Leandro
 */
public class NotPojo {

    private int simpleField;
    private Pojo complexField1;

    public NotPojo(int simpleField, Pojo complexField1) {
        this.simpleField = simpleField;
        this.complexField1 = complexField1;
    }

    public int getSimpleField() {
        return simpleField;
    }

    public void setSimpleField(int simpleField) {
        this.simpleField = simpleField;
    }

    public Pojo getComplexField1() {
        return complexField1;
    }

    public void setComplexField1(Pojo complexField1) {
        this.complexField1 = complexField1;
    }

}
