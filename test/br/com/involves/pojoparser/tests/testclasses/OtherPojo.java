/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.involves.pojoparser.tests.testclasses;

/**
 *
 * @author Leandro
 */
public class OtherPojo {

    private boolean atribute1;
    private String atribute2;
    private byte atribute3;

    public OtherPojo(boolean atribute1, String atribute2, byte atribute3) {
        this.atribute1 = atribute1;
        this.atribute2 = atribute2;
        this.atribute3 = atribute3;
    }

    public boolean isAtribute1() {
        return atribute1;
    }

    public void setAtribute1(boolean atribute1) {
        this.atribute1 = atribute1;
    }

    public String getAtribute2() {
        return atribute2;
    }

    public void setAtribute2(String atribute2) {
        this.atribute2 = atribute2;
    }

    public byte getAtribute3() {
        return atribute3;
    }

    public void setAtribute3(byte atribute3) {
        this.atribute3 = atribute3;
    }

}
