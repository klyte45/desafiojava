/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.involves.pojoparser.tests;

import br.com.involves.pojoparser.PojoFormatter;
import br.com.involves.pojoparser.formats.impl.CSVFormatter;
import br.com.involves.pojoparser.tests.testclasses.NotPojo;
import br.com.involves.pojoparser.tests.testclasses.OtherPojo;
import br.com.involves.pojoparser.tests.testclasses.Pojo;
import java.io.FileOutputStream;
import java.io.OutputStream;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Leandro
 */
public class PojoFormatterTest {

    private PojoFormatter parser;
    private CSVFormatter csvFormatter;
    private Pojo pojo;
    private Pojo[] pojoList;
    private NotPojo notPojo;
    private NotPojo[] notPojoList;
    private Object[] mixedItemsList;

    public PojoFormatterTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        parser = new PojoFormatter();
        csvFormatter = new CSVFormatter();
        pojo = new Pojo("Val1", 0xff, 0xFFFFFFFFFFl);
        pojoList = new Pojo[2];
        pojoList[0] = pojo;
        pojoList[1] = new Pojo("Val4", 0x1f, 0x9787978FFFFFl);
        notPojo = new NotPojo(50, pojo);
        notPojoList = new NotPojo[2];
        notPojoList[0] = notPojo;
        notPojoList[1] = new NotPojo(54, pojo);
        mixedItemsList = new Object[2];
        mixedItemsList[0] = pojo;
        mixedItemsList[1] = new OtherPojo(true, "ADASDA", (byte) 0xff);
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testWithPojo() {
        OutputStream result = null;
        try {
            result = parser.pojoToFileFormat(pojo, csvFormatter);
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse(true);
            return;
        }
        assert result != null;
        System.out.println(result);
        System.out.println(result.toString());
    }

    @Test
    public void testWithPojoList() {
        OutputStream result = null;
        try {
            result = parser.pojoToFileFormat(pojoList, csvFormatter);
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse(true);
            return;
        }
        assert (result != null);
        System.out.println(result.toString());
    }

    @Test
    public void testWithNotPojo() {
        OutputStream result = null;
        try {
            result = parser.pojoToFileFormat(notPojo, csvFormatter);
        } catch (IllegalArgumentException e) {
            assertTrue(true);
            return;
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse(true);
            return;
        }
        assertFalse(true);
    }

    @Test
    public void testWithNotPojoList() {
        OutputStream result = null;
        try {
            result = parser.pojoToFileFormat(notPojoList, csvFormatter);
        } catch (IllegalArgumentException e) {
            assertTrue(true);
            return;
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse(true);
            return;
        }
        assertFalse(true);
    }

    @Test
    public void testWithMixedPojoList() {
        OutputStream result = null;
        try {
            result = parser.pojoToFileFormat(mixedItemsList, csvFormatter);
        } catch (IllegalArgumentException e) {
            assertTrue(true);
            return;
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse(true);
            return;
        }
        assertFalse(true);
    }
}
