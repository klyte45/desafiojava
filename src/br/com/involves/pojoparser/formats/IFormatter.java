package br.com.involves.pojoparser.formats;

import java.io.OutputStream;
import java.util.List;

/**
 *
 * @author Leandro
 */
public interface IFormatter {
    public OutputStream dataToOutput(List<String> fields, List<List<String>> values);
}
