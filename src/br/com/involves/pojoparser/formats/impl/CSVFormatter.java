/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.involves.pojoparser.formats.impl;

import br.com.involves.pojoparser.formats.IFormatter;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.StringJoiner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Leandro
 */
public class CSVFormatter implements IFormatter {

    @Override
    public OutputStream dataToOutput(List<String> fields, List<List<String>> values) {
        Logger.getLogger(CSVFormatter.class.getName()).log(Level.FINE, String.format("Initializing CSVFormatter dataToOutput method"));
        Logger.getLogger(CSVFormatter.class.getName()).log(Level.FINE, String.format("Initializing buffer"));
        StringBuilder buffer = new StringBuilder();

        Logger.getLogger(CSVFormatter.class.getName()).log(Level.FINE, String.format("Printing field list header"));
        StringJoiner fieldTitleJoiner = new StringJoiner(",");
        for (String field : fields) {
            fieldTitleJoiner.add("\"" + field.replaceAll("\"", "\\\"") + "\"");
        }
        buffer.append(fieldTitleJoiner.toString());
        buffer.append("\r\n");

        Logger.getLogger(CSVFormatter.class.getName()).log(Level.FINE, String.format("Printing values"));
        for (List<String> valueFields : values) {
            StringJoiner valueFieldsJoiner = new StringJoiner(",");
            for (String value : valueFields) {
                valueFieldsJoiner.add("\"" + value.replaceAll("\"", "\\\"") + "\"");
            }
            buffer.append(valueFieldsJoiner.toString());
            buffer.append("\r\n");
        }

        OutputStream output = null;
        try {
            Logger.getLogger(CSVFormatter.class.getName()).log(Level.FINE, String.format("Creating outputStream"));
            output = new ByteArrayOutputStream();

            Logger.getLogger(CSVFormatter.class.getName()).log(Level.FINE, String.format("Putting data on stream"));
            output.write(buffer.toString().getBytes());
        } catch (FileNotFoundException ex) {
            Logger.getLogger(CSVFormatter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(CSVFormatter.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                output.close();
            } catch (IOException ex) {
                Logger.getLogger(CSVFormatter.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return output;
    }

}
