package br.com.involves.pojoparser;

import br.com.involves.pojoparser.formats.IFormatter;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Leandro
 */
public class PojoFormatter {

    /**
     * Parse a single pojo to a desired IParseFormat implementation.
     *
     * @param pojo the item to be formated
     * @param formatter the formatter implementation
     * @return OutputStream with the formatter result
     */
    public OutputStream pojoToFileFormat(Object pojo, IFormatter formatter) {
        return pojoToFileFormat(new Object[]{pojo}, formatter);
    }

    /**
     * Parse a pojo list to a desired IParseFormat implementation.
     *
     * @param pojoArray the array with the objects that will be formatted
     * @param formatter the formatter implementation
     * @return OutputStream with the formatter result
     */
    public OutputStream pojoToFileFormat(Object[] pojoArray, IFormatter formatter) {
        Logger.getLogger(PojoFormatter.class.getName()).log(Level.FINE, String.format("Initializing method pojoToFileFormat(Object[] pojoArray, IParseFormat formatter)"));
        if (pojoArray.length == 0) {
            Logger.getLogger(PojoFormatter.class.getName()).log(Level.SEVERE, String.format("The array must have elements!"));
            throw new IllegalArgumentException(String.format("The array must have elements!"));
        }

        Logger.getLogger(PojoFormatter.class.getName()).log(Level.FINE, String.format("Getting pojo's class from first element"));
        Class pojoClass = pojoArray[0].getClass();

        Logger.getLogger(PojoFormatter.class.getName()).log(Level.FINE, String.format("Checking class inheritance"));
        if (pojoClass.getAnnotations().length > 0 || pojoClass.getClasses().length > 0) {
            Logger.getLogger(PojoFormatter.class.getName()).log(Level.SEVERE, String.format("The array class isn't a POJO - it's implements interface or extends superclass or have declared annotations"));
            throw new IllegalArgumentException(String.format("The array class isn't a POJO - it's implements interface or extends superclass or have declared annotations"));
        }

        Logger.getLogger(PojoFormatter.class.getName()).log(Level.FINE, String.format("Getting pojo's class' fields"));
        Field[] fields = pojoClass.getDeclaredFields();

        Logger.getLogger(PojoFormatter.class.getName()).log(Level.FINE, String.format("Initializing arraylists"));
        List<String> fieldNames = new ArrayList<>(fields.length);

        Logger.getLogger(PojoFormatter.class.getName()).log(Level.FINE, String.format("Checking fields"));
        for (Field field : fields) {
            Logger.getLogger(PojoFormatter.class.getName()).log(Level.FINER, String.format("LOOP: Field %s", field.getName()));
            if (field.getType().equals(String.class) || field.getType().isPrimitive()) {
                Logger.getLogger(PojoFormatter.class.getName()).log(Level.FINER, String.format("LOOP: Field %s is a valid field; adding to field name list", field.getName()));
                fieldNames.add(field.getName());
            } else {
                Logger.getLogger(PojoFormatter.class.getName()).log(Level.WARNING, String.format("The class must only have primitive or String properties. The class of field '%s' is '%s'; throwing exception", field.getName(), field.getType().getName()));
                throw new IllegalArgumentException(String.format("The class must only have primitive or String properties. The class of field '%s' is '%s'", field.getName(), field.getType().getName()));
            }
        }
        Logger.getLogger(PojoFormatter.class.getName()).log(Level.FINE, String.format("All fields are valid."));

        Logger.getLogger(PojoFormatter.class.getName()).log(Level.FINE, String.format("Parsing values into values array"));
        List<List<String>> valuesList = new ArrayList<>(pojoArray.length);
        for (Object o : pojoArray) {
            if (!o.getClass().equals(pojoClass)) {
                Logger.getLogger(PojoFormatter.class.getName()).log(Level.SEVERE, String.format("All elements of pojo array must be from the same class"));
                throw new IllegalArgumentException(String.format("The array must have elements!"));
            }
            List<String> values = new ArrayList<>(fields.length);
            for (Field field : fields) {
                String fieldName = field.getName();
                String getterMethodName = "get" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1, fieldName.length());
                try {
                    values.add(pojoClass.getMethod(getterMethodName).invoke(o).toString());
                } catch (IllegalArgumentException ex) {
                    Logger.getLogger(PojoFormatter.class.getName()).log(Level.SEVERE, String.format("Error invoking getter '%s': %s", getterMethodName, ex.getMessage()));
                    throw ex;
                } catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException | SecurityException ex) {
                    Logger.getLogger(PojoFormatter.class.getName()).log(Level.SEVERE, String.format("The getter of the field '%s' (%s.%s) doesn't exists or isn't accessible!", getterMethodName, pojoClass.toString(), fieldName));
                    throw new IllegalArgumentException(String.format("The getter of the field '%s' (%s.%s) doesn't exists or isn't accessible!", getterMethodName, pojoClass.toString(), fieldName), ex);
                }
            }
            valuesList.add(values);
        }

        Logger.getLogger(PojoFormatter.class.getName()).log(Level.FINE, String.format("Sending data to IParseFormat implementation to get the OutputStream and returning."));
        return formatter.dataToOutput(fieldNames, valuesList);
    }
}
